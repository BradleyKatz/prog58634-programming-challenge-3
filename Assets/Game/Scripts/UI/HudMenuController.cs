﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HudMenuController : Menu
{
    [SerializeField]
    private TextMeshProUGUI timeCounterText;
    [SerializeField]
    private TextMeshProUGUI coinCounterText;

    public override void onHideMenu()
    {
        base.onHideMenu();
    }

    public override void onShowMenu()
    {
        base.onShowMenu();
    }

    public override void Start()
    {
        base.Start();
    }

    public void OnCoinCounterChanged()
    {
        if (coinCounterText)
        {
            coinCounterText.SetText(GameStateManager.Instance.Coins.ToString("D3"));
        }
    }

    public void OnTimeCounterChanged()
    {
        if (timeCounterText)
        {
            timeCounterText.SetText(GameStateManager.Instance.ElapsedTime.ToString("D3"));
        }
    }

    private void OnDestroy()
    {
        MenuManager.Instance.removeMenu(menuClassifier);
    }
}
