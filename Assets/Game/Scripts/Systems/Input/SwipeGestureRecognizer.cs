﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SwipeGestureRecognizer : MonoBehaviour
{
    public float swipeRecognitionTime = 0.2f;
    private float currentSwipeTime = 0.0f;

    private class OnSwipeEvent : UnityEvent { }
    private OnSwipeEvent onSwipeEvent = new OnSwipeEvent(); 

    private Rect rightSideRect = new Rect(Screen.width / 2.0f, 0.0f, Screen.width / 2.0f, Screen.height);

    private void Start()
    {
    }

    private void Update()
    {
        Touch? swipeTouch = null;
        foreach (var touch in Input.touches)
        {
           if (rightSideRect.Contains(touch.position))
            {
                swipeTouch = touch;
                break;
            }
        }

        if (swipeTouch.HasValue)
        {
            if (swipeTouch.Value.phase == TouchPhase.Moved)
            {
                currentSwipeTime += swipeTouch.Value.deltaTime;

                Plane swipePlane = new Plane(Camera.main.transform.forward * -1, transform.position);
                Ray ray = Camera.main.ScreenPointToRay(swipeTouch.Value.position);

                float rayDistance;
                if (swipePlane.Raycast(ray, out rayDistance))
                {
                    transform.position = ray.GetPoint(rayDistance);
                }
            }
            else if (swipeTouch.Value.phase == TouchPhase.Ended)
            {
                if (currentSwipeTime >= swipeRecognitionTime)
                {
                    onSwipeEvent.Invoke();
                }
            }
        }
        else
        {
            currentSwipeTime = 0.0f;
        }
    }

    public void AddSwipeRecognizedCallback(UnityAction callback)
    {
        onSwipeEvent.AddListener(callback);
    }

    public void RemoveSwipeRecognizedCallback(UnityAction callback)
    {
        onSwipeEvent.RemoveListener(callback);
    }
}
