﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(AnimationListener))]
[RequireComponent(typeof(BoxCollider2D))]
public class TrapFlameController : MonoBehaviour
{
    private int ANIM_trap_flame = Animator.StringToHash("TrapFlame");

    private AnimationListener animationListener = null;
    private BoxCollider2D box2D = null;

    private void Start()
    {
        box2D = GetComponent<BoxCollider2D>();

        animationListener = GetComponent<AnimationListener>();
        animationListener.AddAnimationCompletedListener(ANIM_trap_flame, new UnityAction<int>(OnFireAnimationCompleted));
    }

    private void OnFireAnimationCompleted(int hashCode)
    {
        FireTrapController fireTrap = transform.parent.GetComponent<FireTrapController>();
        if (fireTrap)
        {
            fireTrap.currentTrapState = FireTrapController.eFireTrapState.Standby;
        }

        this.gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        IsPlayer isPlayer = collision.gameObject.GetComponent<IsPlayer>();
        if (isPlayer)
        {
            // Trigger player death animation
            Animator playerAnimator = isPlayer.GetComponent<Animator>();
            if (playerAnimator)
            {
                playerAnimator.SetTrigger("IsDead");

                AudioManager.Instance.StopBGM();
                AudioManager.Instance.PlaySFX(AudioManager.eSFX.Death);
            }

            // Disable player movement
            NinjaMovementController playerMovement = isPlayer.GetComponent<NinjaMovementController>();
            if (playerMovement)
            {
                playerMovement.moveEnabled = false;
            }

            // Reset game state
            GameStateManager.Instance.OnGameOver();
        }
    }
}
