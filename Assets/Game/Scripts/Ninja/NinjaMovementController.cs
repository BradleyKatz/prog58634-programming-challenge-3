﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]
public class NinjaMovementController : MonoBehaviour
{
    [SerializeField]
    private RuntimeAnimatorController frontAnimator;
    [SerializeField]
    private AnimatorOverrideController backAnimator;
    [SerializeField]
    private AnimatorOverrideController leftAnimator;
    [SerializeField]
    private AnimatorOverrideController rightAnimator;

    private Animator animator;
    private Rigidbody2D rb2d;

    public bool moveEnabled = true;
    public float movementSpeed = 0.5f;

    public enum eMovementDirection
    {
        Front,
        Back,
        Left,
        Right,
    }
    public eMovementDirection facingDirection = eMovementDirection.Front;

    private Dictionary<eMovementDirection, Vector3> moveAxisDict = new Dictionary<eMovementDirection, Vector3>();

    public class MoveDirectionChangedEvent : UnityEvent<eMovementDirection> { }
    public MoveDirectionChangedEvent moveDirectionChangeEvent = new MoveDirectionChangedEvent();

    private void Start()
    {
        animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();

        if (frontAnimator == null)
        {
            Debug.LogWarning("Front animator controller missing on " + gameObject.name);
        }

        if (backAnimator == null)
        {
            Debug.LogWarning("Back animator controller missing on " + gameObject.name);
        }

        if (leftAnimator == null)
        {
            Debug.LogWarning("Left animator controller missing on " + gameObject.name);
        }

        if (rightAnimator == null)
        {
            Debug.LogWarning("Right animator controller missing on " + gameObject.name);
        }

        moveDirectionChangeEvent.AddListener(new UnityAction<eMovementDirection>(OnMoveDirectionChanged));

        moveAxisDict.Add(eMovementDirection.Back, new Vector3(0.0f, 1.0f, 0.0f));
        moveAxisDict.Add(eMovementDirection.Front, new Vector3(0.0f, -1.0f, 0.0f));
        moveAxisDict.Add(eMovementDirection.Left, new Vector3(-1.0f, 0.0f, 0.0f));
        moveAxisDict.Add(eMovementDirection.Right, new Vector3(1.0f, 0.0f, 0.0f));
    }

    private void OnMoveDirectionChanged(eMovementDirection newDirection)
    {
        if (newDirection != facingDirection)
        {
            facingDirection = newDirection;

            switch (facingDirection)
            {
                case eMovementDirection.Front:
                    animator.runtimeAnimatorController = frontAnimator;
                    break;
                case eMovementDirection.Back:
                    animator.runtimeAnimatorController = backAnimator;
                    break;
                case eMovementDirection.Left:
                    animator.runtimeAnimatorController = leftAnimator;
                    break;
                case eMovementDirection.Right:
                    animator.runtimeAnimatorController = rightAnimator;
                    break;
            }
        }
    }

    private void Update()
    {
        if (moveEnabled)
        {
            Vector2 movementAxis = TouchInputManager.Instance.GetJoystickMovementAxis();

            if (movementAxis.y > 0.0f)
            {
                moveDirectionChangeEvent.Invoke(eMovementDirection.Back);
                rb2d.MovePosition(transform.position + (moveAxisDict[eMovementDirection.Back] * movementSpeed));
                animator.SetBool("IsWalking", true);
            }
            else if (movementAxis.y < 0.0f)
            {
                moveDirectionChangeEvent.Invoke(eMovementDirection.Front);
                rb2d.MovePosition(transform.position + (moveAxisDict[eMovementDirection.Front] * movementSpeed));
                animator.SetBool("IsWalking", true);
            }
            else if (movementAxis.x < 0.0f)
            {
                moveDirectionChangeEvent.Invoke(eMovementDirection.Left);
                rb2d.MovePosition(transform.position + (moveAxisDict[eMovementDirection.Left] * movementSpeed));
                animator.SetBool("IsWalking", true);
            }
            else if (movementAxis.x > 0.0f)
            {
                moveDirectionChangeEvent.Invoke(eMovementDirection.Right);
                rb2d.MovePosition(transform.position + (moveAxisDict[eMovementDirection.Right] * movementSpeed));
                animator.SetBool("IsWalking", true);
            }
            else
            {
                animator.SetBool("IsWalking", false);
            }
        }
        else
        {
            animator.SetBool("IsWalking", false);
        }
    }
}
