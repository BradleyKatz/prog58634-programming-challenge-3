﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    public enum eSFX
    {
        SwordSwing,
        Fireball,
        Death,
        FireTrap,
        CoinCollected
    }

    [SerializeField]
    private AudioSource bgmSource = null;

    [SerializeField]
    private AudioSource sfxSource = null;

    [SerializeField]
    private AudioClip[] sfxClips;

    public void PlayBGM(bool loop = true)
    {
        if (bgmSource)
        {
            bgmSource.loop = loop;

            if (bgmSource.isPlaying)
            {
                bgmSource.Stop();
            }

            bgmSource.Play();
            
        }
    }

    public void StopBGM()
    {
        if (bgmSource)
        {
            bgmSource.Stop();
        }
    }

    public void PlaySFX(eSFX sfxID)
    {
        if (sfxSource)
        {
            sfxSource.PlayOneShot(sfxClips[(int)sfxID]);
        }
    }
}
