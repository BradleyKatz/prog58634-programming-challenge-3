﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(NinjaMovementController))]
[RequireComponent(typeof(AnimationListener))]
public class NinjaAttackController : MonoBehaviour
{
    // This needs to be the name of the state, not the animation clip
    private int ANIM_attack_back = Animator.StringToHash("Attacking");

    private Animator animator;
    private NinjaMovementController ninjaMovement;
    private AnimationListener animationListener;

    public GameObject fireballPrefab = null;

    [Tooltip("The time in seconds between spawning one fireball and the next")]
    public float fireballSpawnDelay = 0.5f;

    private Vector3 fireballSpawnPos = Vector3.zero;
    private Quaternion fireballSpawnRotation = Quaternion.identity;
    private bool isSpawningFireball = false;

    void Start()
    {
        animator = GetComponent<Animator>();
        ninjaMovement = GetComponent<NinjaMovementController>();
        animationListener = GetComponent<AnimationListener>();

        animationListener.AddAnimationCompletedListener(ANIM_attack_back, new UnityAction<int>((int hashCode) => ninjaMovement.moveEnabled = true));
        TouchInputManager.Instance.AddSwipeRecognizedCallback(Attack);
    }

    private void Attack()
    {
        if (ninjaMovement.moveEnabled)
        {
            AudioManager.Instance.PlaySFX(AudioManager.eSFX.SwordSwing);

            ninjaMovement.moveEnabled = false;
            animator.SetTrigger("AttackButtonPressed");

            if (fireballPrefab != null && !isSpawningFireball)
            {
                fireballSpawnPos = Vector3.zero;

                switch (ninjaMovement.facingDirection)
                {
                    case NinjaMovementController.eMovementDirection.Front:
                        fireballSpawnPos.y = -1.0f;
                        fireballSpawnRotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, 0.0f));
                        break;
                    case NinjaMovementController.eMovementDirection.Back:
                        fireballSpawnPos.y = 1.0f;
                        fireballSpawnRotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, 180.0f));
                        break;
                    case NinjaMovementController.eMovementDirection.Left:
                        fireballSpawnPos.x = -1.0f;
                        fireballSpawnRotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, 270.0f));
                        break;
                    case NinjaMovementController.eMovementDirection.Right:
                        fireballSpawnPos.x = 1.0f;
                        fireballSpawnRotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, 90.0f));
                        break;
                }

                StartCoroutine(SpawnFireball());
            }
        }
    }

    private IEnumerator SpawnFireball()
    {
        isSpawningFireball = true;

        yield return new WaitForSeconds(fireballSpawnDelay);

        GameObject fireball = Instantiate(fireballPrefab, transform.position + fireballSpawnPos, fireballSpawnRotation);
        FireballController fireballController = fireball.GetComponent<FireballController>();
        if (fireballController)
        {
            fireballController.moveDirection = ninjaMovement.facingDirection;
        }

        AudioManager.Instance.PlaySFX(AudioManager.eSFX.Fireball);

        isSpawningFireball = false;
    }
}
