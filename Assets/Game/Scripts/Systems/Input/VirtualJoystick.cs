﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VirtualJoystick : MonoBehaviour, IDragHandler, IEndDragHandler
{
    [SerializeField]
    private Image joystickBase = null;
    [SerializeField]
    private Image joystickTouchpad = null;

    public float deadzone = 0.5f;

    private Vector2 inputDirection = Vector3.zero;
    public Vector2 InputDirection
    {
        get
        {
            return inputDirection;
        }
    }

    private void Start()
    {
        if (joystickBase == null)
        {
            Debug.LogWarning("Virtual Joystick base image reference is not set");
        }

        if (joystickTouchpad == null)
        {
            Debug.LogWarning("Virtual Joystick touchpad image reference is not set");
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (joystickBase != null && joystickTouchpad != null)
        {
            // Determine the direction of the current drag gesture
            Vector2 dragAxis = Vector2.zero;
            {
                RectTransformUtility.ScreenPointToLocalPointInRectangle(joystickBase.rectTransform, eventData.position, eventData.pressEventCamera, out dragAxis);

                dragAxis.x /= joystickBase.rectTransform.sizeDelta.x;
                dragAxis.y /= joystickBase.rectTransform.sizeDelta.y;
                dragAxis = (dragAxis.magnitude > 1.0f) ? dragAxis.normalized : dragAxis;
            }

            // Determine the actual change in position of the joystick
            Vector2 deltaPosition = Vector2.zero;
            {
                // Deadzone calculations
                {
                    if (dragAxis.x > deadzone || dragAxis.x < -deadzone)
                    {
                        deltaPosition.x = dragAxis.x;
                    }

                    if (dragAxis.y > deadzone || dragAxis.y < -deadzone)
                    {
                        deltaPosition.y = dragAxis.y;
                    }
                }

                // Clamp deltaPosition to be in a four-axis system, no diagonals
                {
                    if (Mathf.Abs(deltaPosition.x) > Mathf.Abs(deltaPosition.y))
                    {
                        deltaPosition.y = 0.0f;
                    }
                    else if (Mathf.Abs(deltaPosition.y) > Mathf.Abs(deltaPosition.x))
                    {
                        deltaPosition.x = 0.0f;
                    }
                }
            }

            // Update inputDirection and the draw position of the joystick
            joystickTouchpad.rectTransform.anchoredPosition = new Vector3(deltaPosition.x * (joystickBase.rectTransform.sizeDelta.x / 3.0f), deltaPosition.y * (joystickBase.rectTransform.sizeDelta.y / 3.0f));
            inputDirection = deltaPosition.normalized;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (joystickTouchpad != null)
        {
            inputDirection = Vector2.zero;
            joystickTouchpad.rectTransform.anchoredPosition = Vector3.zero;
        }
    }
}
