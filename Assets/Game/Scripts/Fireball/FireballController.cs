﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class FireballController : MonoBehaviour
{
    private Rigidbody2D rb2d = null;

    public NinjaMovementController.eMovementDirection moveDirection = NinjaMovementController.eMovementDirection.Front;
    public float moveSpeed = 0.5f;

    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        Vector3 moveAxis = Vector3.zero;

        switch (moveDirection)
        {
            case NinjaMovementController.eMovementDirection.Front:
                moveAxis.y = -1.0f;       
                break;
            case NinjaMovementController.eMovementDirection.Back:
                moveAxis.y = 1.0f;
                break;
            case NinjaMovementController.eMovementDirection.Left:
                moveAxis.x = -1.0f;
                break;
            case NinjaMovementController.eMovementDirection.Right:
                moveAxis.x = 1.0f;
                break;
        }
        
        rb2d.MovePosition(transform.position + (moveAxis * moveSpeed));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        SkullController skull = collision.gameObject.GetComponent<SkullController>();
        if (skull)
        {
            skull.BreakSkull();
        }

        Destroy(this.gameObject);
    }
}
