﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkullController : MonoBehaviour
{
    public GameObject coinPrefab = null;

    public void BreakSkull()
    { 
        if (coinPrefab != null)
        {
            Instantiate(coinPrefab, transform.position, Quaternion.identity);
            Destroy(this.gameObject);
        }
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    IsFireball isFireball = collision.gameObject.GetComponent<IsFireball>();
    //    if (isFireball && coinPrefab != null)
    //    {
    //        Instantiate(coinPrefab, transform.position, Quaternion.identity);
    //        Destroy(this.gameObject);
    //    }
    //}
}
