﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireTrapController : MonoBehaviour
{
    public enum eFireTrapState
    {
        Standby,
        Engaged,
    }
    public eFireTrapState currentTrapState = eFireTrapState.Standby;

    public GameObject firePrefab = null;
    public float maxSearchDistance = 5.0f;

    private Animator fireAnimator = null;

    private void Start()
    {
        if (firePrefab == null)
        {
            Debug.LogWarning("Fire prefab missing on " + gameObject.name);
        }
        else
        {
            fireAnimator = firePrefab.GetComponent<Animator>();
        }
    }

    private void Update()
    {
        if (currentTrapState == eFireTrapState.Standby && !GameStateManager.Instance.IsGameOver)
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, -transform.up, maxSearchDistance, LayerMask.GetMask("Player"));
            if (hit.collider != null)
            {
                currentTrapState = eFireTrapState.Engaged;
                firePrefab.SetActive(true);
                fireAnimator.SetTrigger("TriggerFlame");

                AudioManager.Instance.PlaySFX(AudioManager.eSFX.FireTrap);
            }
        }
    }
}
