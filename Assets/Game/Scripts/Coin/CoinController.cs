﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        IsPlayer isPlayer = collision.gameObject.GetComponent<IsPlayer>();
        if (isPlayer)
        {
            // Play collection sound
            AudioManager.Instance.PlaySFX(AudioManager.eSFX.CoinCollected);

            // Update Game Manager to display coin total
            GameStateManager.Instance.IncrementCoins();

            Destroy(this.gameObject);
        }
    }
}
