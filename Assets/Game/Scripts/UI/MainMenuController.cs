﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuController : Menu
{
    [SerializeField]
    private Button playButton;
    [SerializeField]
    private Button exitButton;
    
    public SceneReference gameScene;

    public override void onHideMenu()
    {
        base.onHideMenu();
    }

    public override void onShowMenu()
    {
        base.onShowMenu();
    }

    public override void Start()
    {
        base.Start();

        if (playButton)
        {
            playButton.onClick.AddListener(OnStartButtonClicked);
        }
        else
        {
            Debug.LogWarning("Main Menu play button reference is not set");
        }

        if (exitButton)
        {
            exitButton.onClick.AddListener(OnExitButtonClicked);
        }
        else
        {
            Debug.LogWarning("Main Menu exit button reference is not set");
        }
    }

    private void OnStartButtonClicked()
    {
        Scene _scene = SceneManager.GetSceneByName(gameScene);
        if (_scene.isLoaded == false)
        {
            Debug.Log("Loading Scene: " + gameScene);

            SceneLoader.Instance.UnLoadCurrentScene();
            SceneLoader.Instance.LoadLevel(gameScene, true, "");
        }
    }

    private void OnExitButtonClicked()
    {
        Application.Quit();
    }
}
