﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

[RequireComponent(typeof(SwipeGestureRecognizer))]
public class TouchInputManager : Singleton<TouchInputManager>
{
    [SerializeField]
    private VirtualJoystick joystick = null;
    [SerializeField]
    private SwipeGestureRecognizer swipeGestureRecognizer = null;

    private void Start()
    {
        if (joystick == null)
        {
            Debug.LogWarning("Virtual joystick reference not set on TouchInputManager");
        }

        if (swipeGestureRecognizer == null)
        {
            Debug.LogWarning("Swipe Gesture Recognizer instance not set on TouchInputManager");
        }
    }

    public Vector2 GetJoystickMovementAxis()
    {
        if (joystick != null)
        {
            return joystick.InputDirection;
        }
        else
        {
            return Vector2.zero;
        }
    }

    public void AddSwipeRecognizedCallback(UnityAction callback)
    {
        if (swipeGestureRecognizer != null)
        {
            swipeGestureRecognizer.AddSwipeRecognizedCallback(callback);
        }
    }

    public void RemoveSwipeRecognizedCallback(UnityAction callback)
    {
        if (swipeGestureRecognizer != null)
        {
            swipeGestureRecognizer.RemoveSwipeRecognizedCallback(callback);
        }
    }
}
