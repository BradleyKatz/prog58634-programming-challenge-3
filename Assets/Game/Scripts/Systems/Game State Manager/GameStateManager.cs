﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateManager : Singleton<GameStateManager>
{
    private static int maxTime = 999;
    private static int maxCoins = 999;
    private static float resetDelay = 3.0f;

    private void Start()
    {
        AudioManager.Instance.PlayBGM();
    }

    [SerializeField]
    private MenuClassifier hudMenuClassifier = null;
    private HudMenuController hudMenu = null;

    #region Coins
    private int coins = 0;
    public int Coins
    {
        get
        {
            return coins;
        }
    }

    public void IncrementCoins()
    {
        ++coins;
        if (coins > maxCoins)
        {
            coins = maxCoins;
        }

        if (hudMenu)
        {
            hudMenu.OnCoinCounterChanged();
        }
    }
    #endregion

    #region GameOver
    private bool isGameOver = false;
    public bool IsGameOver
    {
        get
        {
            return isGameOver;
        }
    }

    public void OnGameOver()
    {
        isGameOver = true;
        StartCoroutine(DelayedReset());
    }

    private IEnumerator DelayedReset()
    {
        yield return new WaitForSeconds(resetDelay);

        SceneLoader.Instance.UnLoadCurrentScene();
        SceneLoader.Instance.LoadLevel("Game", true);
    }
    #endregion

    #region ElapsedTime
    private float elapsedTime = 0.0f;
    public int ElapsedTime
    {
        get
        {
            return System.Convert.ToInt32(elapsedTime % 60);
        }
    }

    private void Update()
    {
        if (hudMenuClassifier != null && hudMenu == null)
        {
            hudMenu = MenuManager.Instance.getMenu<HudMenuController>(hudMenuClassifier);
        }
        else
        {
            elapsedTime += Time.deltaTime;

            if (hudMenu)
            {
                hudMenu.OnTimeCounterChanged();
            }
        }
    }
    #endregion
}
